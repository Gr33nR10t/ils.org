<!--
.. title: ConfSL 2015
.. slug: confsl-2015
.. date: 2015-05-07 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Il 26 e 27 giugno torna la <a rel="nofollow" href="http://confsl.org/confsl15/" rel="nofollow">ConfSL</a>, l'annuale conferenza nazionale sul software libero, quest'anno a Ivrea (TO). Una occasione per fare il punto sul movimento freesoftware (e non solo software) in Italia, sui contenuti e sui traguardi, sulle strategie e sui problemi.

Come sempre il programma, in fase di perfezionamento, prevede interventi di numerosi esponenti del nostro piccolo grande mondo, ed in particolare nella seconda giornata è previsto un talk di Richard Stallman, che del movimento freesoftware è stato il fondatore.

Naturalmente all'evento sarà presente anche Italian Linux Society, in rappresentanza della capillare <a href="https://lugmap.linux.it/">rete di Linux Users Groups</a> distribuita su tutto il territorio, e ci auguriamo di trovare anche voi. Non solo per ascoltare, ma soprattutto per dire la vostra.