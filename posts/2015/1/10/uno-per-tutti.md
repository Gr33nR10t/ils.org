<!--
.. title: Uno per Tutti
.. slug: uno-per-tutti
.. date: 2015-01-10 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

In Italia, contrariamente a molti altri Paesi, ci sono <a href="https://lugmap.linux.it/regione.php">dozzine di gruppi locali</a> di promozione a Linux ed al software libero che conducono le <a href="https://lugmap.linux.it/eventi/" rel="nofollow">attività</a> più disparate: corsi, sportelli periodici di assistenza, il <a href="https://www.linuxday.it/">Linux Day</a> e diversi altri tipi di incontri e iniziative.

Non tutti sono costituiti in associazioni, e spesso - soprattutto tra i più giovani - si preferisce un approccio informale: un gruppo di persone, una mailing list, e chi vuole può partecipare anche solo occasionalmente. Tale assetto, assolutamente lecito e legittimo ed anzi consigliabile appunto a chi sta avviando un nuovo LUG e non vuole barcamenarsi tra gli adempimenti burocratici di una associazione propriamente detta, presenta un unico grosso limite: non esistendo un soggetto giuridico di riferimento è più difficile avere rapporti col proprio Comune e con altri enti, sia statali che di altro genere.

Ed è qui che interviene Italian Linux Society, nella sua perenne missione di dare supporto alle realtà che agiscono sul territorio in nome del software libero. Laddove esista un gruppo informale che voglia relazionarsi con le proprie istituzioni locali, magari per chiedere un patrocinio, o degli spazi, o per partecipare ad un progetto cittadino strutturato, ILS si offre come soggetto di riferimento cui intestare procedimenti, carte, moduli, ed insomma cui delegare le incombenze amministrative formali. In questo modo sarà possibile avere una realtà giuridica di appoggio, e di fatto agire con i mezzi e l'autorità di una associazione a pieno titolo pur non dovendo assumerne direttamente gli oneri.

Per accedere a tale opportunità è richiesto che almeno un membro del gruppo sia <a href="/iscrizione">iscritto</a> ad Italian Linux Society, affinché ne sia portavoce e referente locale, e sottoporre le proprie richieste al Consiglio Direttivo (all'indirizzo <a href="mailto:ils-cd@linux.it">ils-cd@linux.it</a>). Per i gruppi che invece già sono costituiti in associazione, ricordiamo la possibilità di aderire alla <a href="/info#aderenti">crescente rete ILS</a> semplicemente sottoponendo richiesta di iscrizione all'indirizzo <a href="mailto:direttore@linux.it">direttore@linux.it</a>.