<!--
.. title: 2021 con ILS
.. slug: 2021-con-ils
.. date: 2022-01-01 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: <a rel="nofollow" href="https://www.flickr.com/photos/51035555243@N01">Thomas Hawk from San Francisco, USA</a>, <a href="https://creativecommons.org/licenses/by/2.0" title="Creative Commons Attribution 2.0">CC BY 2.0</a>, via Wikimedia Commons
.. previewimage: /images/posts/2022.jpg
-->


Si conclude un altro anno, nuovamente reso complicato dalla pandemia ma in cui non sono comunque mancate le iniziative a sostegno di Linux e del software libero.

<!-- TEASER_END -->

Qui un riassunto di quanto compiuto negli scorsi dodici mesi:

* circa 10.000 euro sono stati distribuiti nel corso del 2021, in diverse forme e modalità: una serie di "bounties" <a href="{% link _posts/2021-02-05-caccia-al-bug.md %}">a inizio anno</a>, una donazione <a href="{% link _posts/2021-09-20-sostegno-common-voice.md %}">al progetto Common Voice</a> a settembre, ed in ultimo <a href="{% link _posts/2021-12-29-donazioni-2021.md %}">un massiccio round di donazioni</a> destinate a progetti di sviluppo ed iniziative legate alla cultura libera a dicembre. Approfittiamo dell'occasione per ricordare ancora una volta quanto sia importante e necessario sostenere, anche e soprattutto economicamente, lo sviluppo, la manutezione e la crescita delle soluzioni libere e open source, magari aderendo alla [Campagna 1%](https://www.linux.it/unopercento/) (per le imprese) o sostenendo la stessa ILS, che appunto alloca la stragrande maggioranza delle sue entrate (in donazioni, [sponsorizzazioni](/sponsorizzazioni) e quote di iscrizione dei soci) allo sviluppo attivo
* a maggio si è tenuta <a href="{% link _posts/2021-05-14-mergeit-2021.md %}">la seconda edizione di MERGE-it</a>, la conferenza che aggrega le principali realtà italiane attive nel campo delle libertà digitali, promossa e coordinata da ILS. Nel corso della giornata, trasmessa online in diretta streaming, si sono svolte diverse tavole rotonde tematiche in cui si sono incontrati (e talvolta scontrati) i numerosi punti di vista che animano l'eterogenea community italiana (i cui video sono tutti reperibili [su video.linux.it](https://video.linux.it/videos/recently-added))
* come ogni anno, ad ottobre si è svolto il [Linux Day](https://www.linuxday.it/2021/): dopo l'esperienza totalmente online del 2020 sono ritornate le manifestazioni in presenza in diverse città, mentre per tutti gli altri è stato allestito un evento in diretta streaming articolato in quattro sessioni tematiche e complessivamente con oltre 40 interventi (anche questi video sono [su video.linux.it](https://video.linux.it/videos/recently-added))
* è stato attivato [il Forum](https://forum.linux.it/), nuova piattaforma - un poco più "amichevole" delle classiche mailing list - per aggregare considerazioni e progetti da parte della community freesoftware italiana. La partecipazione è naturalmente aperta a tutti coloro che vogliono prendere parte attiva agli sviluppi, software e non solo software...
* ... tra cui, ad esempio, le <a href="{% link _posts/2021-07-16-eventi-planet-linux-it.md %}">recenti evoluzioni</a> apportate a Planet.Linux.it, aggregatore delle news - e, ora, degli eventi - della community nazionale, per favorire la scoperta di nuovi appuntamenti presso cui incontrare (dal vivo o virtualmente) altri appassionati in tutta Italia
* è stato siglato un protocollo di intesa <a href="{% link _posts/2021-02-02-protocollo-con-informatici-senza-frontiere.md %}">con l'associazione Informatici Senza Frontiere</a> e stretta una partnership <a href="{% link _posts/2021-03-19-lpi-community-partner.md %}">con Linux Professional Institute</a>, per consolidare ancora la rete nazionale (ed internazionale) dedicata alla cultura libera
* le [Sezioni Locali ILS](/sezionilocali) sono diventate 8 (tra cui 4 sono in capoluoghi di regione), ed abbiamo iniziato a <a href="{% link _posts/2021-08-30-sezioni-locali-fondi-siti.md %}">distribuire fondi</a> per agevolare e facilitare le attività locali di promozione e divugazione di Linux e del software libero (quest'anno utilizzati in massima parte per il Linux Day); nonostante la pandemia COVID-19, che spesso ci ha costretti a ricorrere a mezzi di incontro virtuali, ILS non perde la sua vocazione di sostegno operativo al territorio
* un'ultima nota dedicata al rimborso delle licenze Windows e alla sanzione di 20.000 euro <a href="{% link _posts/2021-02-18-risarcimento-20000-euro-lenovo-mancato-rimborso-windows.md %}">imposta a Lenovo</a>, che ha avuto grande seguito a febbraio; per l'occasione il sito [sistemainoperativo.it](https://www.sistemainoperativo.it/) è stato ulteriormente arricchito con una guida pratica su come procedere (anche legalmente) per far riconoscere il proprio diritto al rimborso

Per restare sempre aggiornati su ciò che succede nel mondo del software libero in Italia potete sottoscrivere <a href="/newsletter">la nostra newsletter</a> o seguirci su [Twitter](https://twitter.com/ItaLinuxSociety/), su [Facebook](https://www.facebook.com/italinuxsociety/), su [Instagram](https://www.instagram.com/italinuxsociety/) e su [Mastodon](https://mastodon.uno/@italinuxsociety). Nonché associarvi alla nostra associazione e godere di [tutti i benefici riservati ai soci ILS](/iscrizione).