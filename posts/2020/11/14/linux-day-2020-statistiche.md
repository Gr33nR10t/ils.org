<!--
.. title: Linux Day 2020: Statistiche
.. slug: linux-day-2020-statistiche
.. date: 2020-11-14 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: Photo by <a rel="nofollow" href="https://unsplash.com/@isaacmsmith?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Isaac Smith</a> on <a rel="nofollow" href="https://unsplash.com/s/photos/analysis?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a>
.. previewimage: /images/posts/stats_ld.jpg
-->


Durante il <a href="https://www.linuxday.it/2020/">Linux Day Online 2020</a> è circolato un questionario rivolto ai partecipanti: in 220 lo hanno compilato, e qui riassumiamo alcuni tratti salienti emersi dall'analisi dei risultati.

<!-- TEASER_END -->

<img class="mx-auto" src="/images/posts/ld20_01.svg" loading="lazy">

Di gran lunga, il maggior numero di partecipanti si trova in Lombardia. Che forse non a caso è anche la regione italiana col <a href="https://lugmap.linux.it/statistiche.php">maggior numero di Linux Users Groups</a>. Seguono Sicilia e Lazio, e via via tutte le altre regioni.

<img class="mx-auto" src="/images/posts/ld20_02.svg" loading="lazy">

Il dato in assoluto più eclatante è quello delle persone che hanno partecipato quest'anno per la prima volta al Linux Day, pari a quasi il 40% di tutti coloro che hanno compilato il questionario. Evidentemente la modalità online è riuscita nell'intento di arrivare anche là dove normalmente non arriva il Linux Day fisico, andando a toccare un bacino assai più esteso del solito.

<img class="mx-auto" src="/images/posts/ld20_03.svg" loading="lazy">

Come prevedibile la maggior parte dei partecipanti sono persone che lavorano nel settore IT, e che hanno dunque presumibilmente a che fare con Linux e con strumenti open source ogni giorno per mestiere, ma subito dopo si trova una larghissima fetta di studenti (soprattutto universitari). La partecipazione di altre tipologie di pubblico, benché più modesta, non è comunque trascurabile, in particolare quella degli insegnanti - che sono stati appositamente misurati con una opzione a loro dedicata nel questionario.

Prendendo in esame anche tutti gli altri caratteri rappresentati dal questionario, il pubblico è stato suddiviso in categorie nelle seguenti fasce:

<ul>
    <li>Il primo segmento è composto da studenti, giovani, soprattuto delle regioni del Nord-est e del Centro, veterani del Linux Day (41 questionari).</li>
    <li>Il secondo segmento si caratterizza da occupati nell’IT, relativamente giovani (19-30 anni) che partecipano per la prima volta al Linux Day (28 questionari).</li>
    <li>Il terzo segmento è composto da occupati in settori non-STEM, di età variegata, che per lo più anche loro partecipano al Linux Day per la prima volta (47 questionari).</li>
    <li>Il quarto sono informatici di professione, età dai 31 ai 65 (quindi più navigati del 2º segmento) (circa 60 questionari).</li>
    <li>La quinta categoria, residuale, non rivela particolari qualità.</li>
</ul>

Si ringraziano Francesco Ariis, Stefania Delprete e Andrea Laisa per l'analisi.