<!--
.. title: Linux Day 2020
.. slug: linux-day-2020
.. date: 2020-05-23 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->


Pressoché ogni persona interessata a Linux ed al software libero in Italia conosce il <a href="https://www.linuxday.it/">Linux Day</a>, la manifestazione annuale che - il quarto sabato di ogni ottobre - attiva centinaia di volontari in tutto il Paese con l'obiettivo comune di promuovere, divulgare, informare sulle tematiche della consapevolezza e delle libertà digitali tramite decine di eventi locali che si svolgono in città diverse ma nello stesso giorno.

<!-- TEASER_END -->

Nel 2020 ne ricorre la <strong>ventesima edizione</strong>. Ed un anniversario così eccezionale, che cade in circostanze (quelle imposte dalla diffusione del COVID-19) così eccezionali, non poteva che essere celebrato in modo <strong>eccezionale</strong>.

<strong>Il Linux Day 2020 si svolgerà in forma di manifestazione nazionale, unificata e online</strong>. Articolata su due giorni, <strong>sabato 24 e domenica 25 ottobre</strong>, e su diverse sessioni tematiche popolate da talk e interventi.

Sul sito si trova il form per la <a href="https://www.linuxday.it/2020/partecipa/">Call for Papers</a>, destinata ad accogliere i contributi di coloro che vorranno far parte di questa inedita iniziativa ed intendono accogliere la sfida del palco nazionale virtuale. E, come in ogni Linux Day, sono benvenute proposte di ogni tipo, da quelle divulgative rivolte a coloro che vogliono saperne un poco di più sull'argomento o su programmi freesoftware utili nella vita di tutti i giorni, a quelle intese ad approfondire gli aspetti tecnici di applicazioni e tecnologie open source professionali.

Per ulteriori aggiornamenti ed annunci si raccomanda di seguire i canali social del Linux Day (su <a rel="nofollow" href="https://www.facebook.com/LinuxDayItalia">Facebook</a> e <a rel="nofollow" href="https://twitter.com/linuxdayitalia">Twitter</a>) o di iscriversi alla <a href="/newsletter">newsletter di Italian Linux Society</a>.