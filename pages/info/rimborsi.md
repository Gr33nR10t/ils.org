---
layout: page
title: Rimborsi Spese - Sostegno Economico
description: Italian Linux Society sostiene il software libero investendo le risorse dell'associazione in iniziative promosse da volontari e volontarie.
---
 _Questa pagina è una <abbr title="Request for Comments">RFC</abbr>. commenta la <a href="https://gitlab.com/ItalianLinuxSociety/ils.org/-/merge_requests/43">proposta #43</a> · [contattaci](/contatti/)_

Italian Linux Society sostiene il software libero investendo le risorse dell'associazione in iniziative promosse da volontari e volontarie.

Italian Linux Society è un'associazione di volontariato e non può quindi pagare i volontari. Possono essere sostenuti i costi di acquisto e produzione di materiale promozionale, spese di pulizia delle sale, ecc.

[TOC]

## Requisiti

Le iniziative (es. eventi) devono soddisfare i seguenti requisiti per poter ricevere un sostegno economico:

* L'iniziativa ha un responsabile (proponente). 
* L'iniziativa ha una data di inizio e di fine.
	* esempio: in data XXX si terrà il Linux Day CITTÀ, più un evento accompagnatorio, quest'ultimo in data YYY da confermare ecc.
* L'iniziativa contribuisce agli scopi di Italian Linux Society (elencati nello <a href="/statuto/">statuto</a>)
	* Esempio: organizzare un banchetto dimostrativo su specifici software liberi ad una fiera.
* L'iniziativa presenta il totale di spesa, allega almeno un preventivo, chiarisce le risorse minime necessarie per la realizzazione dell'iniziativa, e indica eventuali costi aggiuntivi opzionali.
	* esempio: per il materiale promozionale, è sufficiente calcolare il loro costo sul sito di un fornitore e allegare uno screenshot del carrello. L'importante è fornire il totale e il costo unitario. In un commento spiega perché ritieni che quelle quantità siano adeguate.
* Gli organizzatori dichiarano i loro eventuali conflitti di interessi.
	* Esempio: se uno dei fornitori suggeriti ha legami di parentela con gli organizzatori, va segnalato e vanno almeno forniti altri preventivi per un adeguato confronto.
* Il proponente si impegna a pubblicare una breve relazione entro tre mesi dal termine dell'iniziativa.
	* Scopo: condividere una descrizione di come si è svolta l'attività, i risultati ottenuti, i problemi emersi e dei miglioramenti da prevedere per simili attività in futuro.
	* Esempio: crea una pagina sul tuo sito o sul forum di ILS sezione Progetti. Ancora meglio se includi qualche foto in licenza libera CC BY-SA e l'inicazione di chi ha realizzato l'immagine.
* Il proponente richiede l'approvazione dal consiglio direttivo.
	* È anche possibile ottenere l'approvazione direttamente da una [sezione locale](/sezionilocali/) di riferimento (se ha sufficienti fondi)

Se hai dubbi sui requisiti, contattaci per chiarimenti (contatti a fondo pagina).

Una volta che il sostegno economico all'iniziativa è approvato, puoi richiedere che sia Italian Linux Society a pagare direttamente le spese (opzione 1 - consigliata), oppure puoi anticipare i pagamenti e ti rimborsiamo (opzione 2):

## Criteri

La priorità delle richieste è definita attraverso i seguenti criteri, dal più importante al meno importante.

* Tutte le persone coinvolte nell'iniziativa fanno volontariato.
	* Attenzione a segnalare eventuali relatori retribuiti; in generale questo non è ammesso.
* La richiesta di sostegno economico è inviata con anticipo. 
	* Le richieste economiche presentate con 3 settimane di anticipo sono avvantaggiate rispetto a quelle presentate 2 giorni prima dell'evento o addirittura 20 minuti dopo aver effettuato la spesa.
* L'iniziativa presenta una richiesta economica accompagnata da più preventivi
	* A chi dona fa piacere sapere che ti stai prendendo cura del tuo budget e che hai valutato almeno un'altra opzione magari potenzialmente migliore. Ogni ulteriore preventivo che vuoi allegare è ben visto. Senza esagerare, tre preventivi massimo.
* L'iniziativa ha un adeguato rapporto qualità/prezzo.
	* anche inteso come risorse investite e risultati attesi o pubblico raggiunto.
* L'iniziativa coinvolge altri partner e co-finanziatori.
	* Aver trovato altri partner (associazioni, università, enti pubblici, media partner ecc.) o altri co-finanziatori (donatori, sponsor, forniture gratuite, fondazioni erogatrici, fondi pubblici) è un'ottima cosa.
	* Chiarisci quali risorse sono già coperte da altre persone e istituzioni (esempio: la sala è offerta da... il pranzo è già offerto da ...)
* L'iniziativa è comunicata e invita alla partecipazione.
	* ottimo se la tua iniziativa è già pubblicata online per richieste di pareri, per esempio sul forum
* L'iniziativa è una richiesta nata internamento a Italian Linux Society.
	* Ottimo se il proponente è una persona associata ad Italian Linux Society
	* Ottimo se il proponente o gli organizzatori fanno parte di una sezione locale
	* Ottimo se si è attivi in un user group riconosciuto (esempio: nella [LugMap](https://lugmap.linux.it/))
* L'iniziativa non include rimborso di viaggi. I rimborsi di viaggio sono raramente finanziati e secondo i seguenti criteri:
	* La persona richiedente sia nell'organizzazione ufficiale dell'evento o almeno sia relatore ufficiale o simile ruolo.
	* treni: preferenza a biglietti in classe economy
	* automobile: da evitare (difficile da quantificare e non sostenibile)
* Sostenibilità ambientale.
	* L'iniziativa considera la sostenibilità ambientale nella produzione di materiale, nei trasporti, ove possibile.

Questi criteri sono utili quando non ci sono sufficienti fondi per coprire tutte le ottime richieste pervenute.

## Come procedere

1. Prima possibile manda una mail al consiglio direttivo per informarlo che vuoi richiedere un sostegno economico
    * Scrivi a ils-cd@linux.it
2. Compila il "modulo dei rimborsi spese" con i dettagli della tua richiesta di sostegno economico e allega i preventivi
    * <https://ilsmanager.linux.it/ng/refund>
3. Nella sezione commenti del formulario, scegli se sarà Italian Linux Society a pagare direttamente le spese (opzione 1 - consigliata), oppure se anticiperai i pagamenti e ti rimborsiamo (opzione 2). 

<div class="row">
	<div class="col s12 m6 card">
		<h3>Opzione 1<br />Acquisto diretto da Italian Linux Society</h3>
		<p><em>Italian Linux Society può eventualmente pagare tutto al posto tuo cosicché non devi anticipare nulla.</em></p>
		<p>
		<b>Vantaggi:</b><br />
			<ul>
				<li>La persona richiedente non anticipa nulla</li>
				<li>È ideale anche per la commercialistica di ILS, perché paga ILS e la fattura è intestata ad ILS.</li>
			</ul>
		<b>Svantaggi:</b><br />
			<ul>
				<li>devi fornire a ILS tutti i dettagli su ciò che serve acquistare (caratteristiche precise, quantità, ecc.) e tutti i dettagli di spedizione</li>
				<li>potrebbero esserci ulteriori rallentamenti di acquisto e quindi di spedizione se la segreteria di ILS è sovraccarica (ma tu non richiedi le cose all'ultimo con urgenza, giusto?)</li>
			</ul>
		</p>
	</div>
	<div class="col s12 m6 card">
		<h3>Opzione 2:<br />Rimborso</h3>
		<p><em>Puoi decidere di pagare tu, effettuando in autonomia i tuoi acquisti (senza doverci dare dettagli di spedizione ecc.) e farti rimborsare successivamente.</em></p>
		<p>
		<b>Vantaggi:</b><br />
			<ul>
				<li>puoi acquistare in maggiore autonomia con la certezza che sia tutto come volevi</li>
				<li>ideale se il carrello è molto complicato e difficilmente condivisibile, o simili situazioni in cui preferisci pagare da te</li>
			</ul>
		<b>Svantaggi:</b><br />
			<ul>
				<li>devi anticipare il pagamento</li>
				<li>devi ottenere una fattura intestata a te o comunque una ricevuta che dimostri l'acquisto da parte tua (per evitare che si rimborsino scontrini anonimi, pagati da altre persone estranee)</li>
				<li>devi fornirci i tuoi estremi bancari per il rimborso</li>
			</ul>
		</p>
	</div>
</div>

Chiarisci quale di queste due preferenze farebbe più comodo. Questa tua scelta può variare per ogni fornitore coinvolto dal tuo progetto.

## Altro

Per motivi legati allo statuto e alle responsabilità del direttivo, il direttivo ha comunque potere di veto sulle richieste, anche quelle rivolte alle sezioni locali.

Per eventuali altre tipologie di richieste di pagamenti/donazioni/acquisto leggermente fuori dagli schemi proposti, è comunque possibile proporre la cosa al direttivo, in base alle necessità, disponibilità e consenso.

Segnaliamo anche altri programmi di sostegno esterni ad Italian Linux Society, da parte di partner convenzionati o simili, e che spesso possono sostenere (o co-sostenere) il tuo progetto:

* [Microgrant di Wikimedia Italia](https://wiki.wikimedia.it/wiki/Microgrant) - per questioni legate a Wikimedia e OpenStreetMap
* [Project Call di FSFE](https://fsfe.org/contact/projects-call/projects-call.it.html) - per software libero, da poter compilare (però in lingua inglese)

## Contattaci

Per domande puoi:

- contattare pubblicamente Italian Linux Society per esempio tramite Telegram FOSS
  sezione "Segreteria" - [https://t.me/ItalianLinuxSociety/45](https://t.me/ItalianLinuxSociety/45)
- oppure contattare in privato direttore@linux.it per un primo parere
- altri [contatti](/contatti/)


